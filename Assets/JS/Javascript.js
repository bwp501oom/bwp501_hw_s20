$( "#SignupNextBtn" ).click(function() {
    var prev = $('.dot');
    var current = $('.dot-inactive');
        $('.content-container-1').fadeOut(300,function() {
        $('.content-container-2').fadeIn(300);
        prev.removeClass('dot').addClass('dot-inactive');
        current.removeClass('dot-inactive').addClass('dot');
    });
});

$( "#SignupPreviousBtn" ).click(function() {
    var prev = $('.dot');
    var current = $('.dot-inactive');
    $('.content-container-2').fadeOut(300,function() {
        $('.content-container-1').fadeIn(300);
        current.removeClass('dot-inactive').addClass('dot');
        prev.removeClass('dot').addClass('dot-inactive');

    });
});

$(".HomeLink").mouseenter(function(){
        $(this).find('.DownArrow').attr('src','../Assets/Icons/DownArrow.gif')
    })
    .mouseleave(function(){
        $(this).find('.DownArrow').attr('src','../Assets/Icons/DownArrow.png')
});
