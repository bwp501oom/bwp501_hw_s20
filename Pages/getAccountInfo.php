<?php
    include('dbConnection.php');
    $Accountid = $_REQUEST['id'];
    if ($Accountid) {
        // get all account information
        $Account = mysqli_query($connection,"SELECT * FROM accounts WHERE id = '" . $Accountid ."' LIMIT 1");
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        // create a new array and add all info, and current city and state and country names for this account
        $AccountArray = array();
        while($acc = mysqli_fetch_assoc($Account)){
            $AccountArray[] = $acc;
        }

        $City = mysqli_query($connection,"SELECT id,state_id FROM city WHERE id = '" . $AccountArray[0]['city'] ."' LIMIT 1");
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        while($St = mysqli_fetch_assoc($City)){
            $AccountArray[] = $St;
        }

        $State = mysqli_query($connection,"SELECT country_id,state FROM state WHERE id = '" . $AccountArray[1]['state_id'] ."' LIMIT 1");
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        while($Sta = mysqli_fetch_assoc($State)){
            $AccountArray[] = $Sta;
        }

        $Country = mysqli_query($connection,"SELECT country FROM country WHERE id = '" . $AccountArray[2]['country_id'] ."' LIMIT 1");
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        while($Count = mysqli_fetch_assoc($Country)){
            $AccountArray[] = $Count;
        }


        // create 2 new arrays to fill select options, one for cities and another for state
        $CityArray = array();
        $allCities = mysqli_query($connection,"SELECT id,state_id,city FROM city where state_id = '" . $AccountArray[1]['state_id'] ."'");
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        while($allCit = mysqli_fetch_assoc($allCities)){
            $CityArray[] = $allCit;
        }

        $StateArray = array();
        $allStates = mysqli_query($connection,"SELECT id, country_id, state FROM state WHERE country_id = '" . $AccountArray[2]['country_id'] ."'");
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        while($allState = mysqli_fetch_assoc($allStates)){
            $StateArray[] = $allState;
        }
        // return all arrays in 1 single array thats easy to use for javascript
        echo json_encode(array($AccountArray, $CityArray,$StateArray));
    }

    // echo $Accountid;
?>
