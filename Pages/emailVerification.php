<!--
SVU - ITE - S20 - BWP501-Project
Dr.Bassel
== User Management Panel ==
//
Participants:
-mhd_hussam_109817
-omar_108591
-omar_116205
//
Last time updated by Hussam Habbas on 20-OCT-29
-->
<?php
   include('master.php');
?>
<!DOCTYPE html>
<html>

<head>
    <title>CPM | Email Verification</title>
</head>

<body>
    <div class="Signup_Page">
        <div class="col-md-6 inner-page" style="background: white; height: 230px; width: 650px; padding-top: 10px;">
            <div class="container-fluid">
                <div class="text-center" style="padding-bottom: 15px; padding-top: 15px;">
                    <img src="../Assets/Icons/email.svg" alt="COPLogo" height="50px" width="70px" />
                </div>
                <div class="row">
                    <div class="col-md-12 text-center content-header">
                        <h2>
                            <b>
                                Check your Email!
                            </b>
                        </h2>

                        <h4 style="padding-top: 15px;">
                            We have sent you an email that contains a verification link,
                            follow it in order to verify your account.
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'javascripts.php';?>
</body>

</html>
