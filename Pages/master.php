<!--
SVU - ITE - S20 - BWP501-Project
Dr.Bassel
== User Management Panel ==
//
Participants:
-mhd_hussam_109817
-omar_108591
-omar_116205
//
Last time updated by Hussam Habbas on 20-OCT-29
-->
<?php $result = array(); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!-- CSS file -->
    <link rel="stylesheet" href="../Assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../Assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../Assets/CSS/all.min.css">
    <link rel="stylesheet" type="text/css" href="../Assets/CSS/all.min.css">
    <!-- JS Files -->
    <script src="..\Assets\JS\jquery.min.js"></script>
    <script src="..\Assets\JS\jsFunctions.js"></script>
    <!-- favicon -->
    <link rel="icon" href="../Assets/Icons/CPMLogo.svg">
</head>

<body>
    <!-- Website Logo -->
    <div class="text-center" style="padding-bottom: 15px; padding-top: 5px;">
        <img src="../Assets\Icons\CPMLogo.svg" alt="COPLogo" height="70px" width="70px" />
    </div>
</body>

</html>
