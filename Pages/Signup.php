<!--
   SVU - ITE - S20 - BWP501-Project
   Dr.Bassel ALKHATIB
   == User Management Panel ==
   //
   Participants:
   -mhd_hussam_109817
   -omar_108591
   -omar_116205
   //
-->
<?php
include('master.php');
include('dbConnection.php');
if (isset($_POST['userFirstName']) && isset($_POST['userMidInitial']) && isset($_POST['userLastName']) && isset($_POST['userCountry']) && isset($_POST['userState']) && isset($_POST['userCity']) && isset($_POST['userZIP']) && isset($_POST['userUsername']) && isset($_POST['userEmail']) && isset($_POST['userPassword1']) && isset($_POST['userPassword2']) && isset($_POST['userAccpted'])) {
    $userFirstName  = $_POST['userFirstName'];
    $userMidInitial = $_POST['userMidInitial'];
    $userLastName   = $_POST['userLastName'];
    $userCountry    = $_POST['userCountry'];
    $userState      = $_POST['userState'];
    $userCity       = $_POST['userCity'];
    $userZIP        = $_POST['userZIP'];
    $userUsername   = $_POST['userUsername'];
    $userEmail      = $_POST['userEmail'];
    $userPassword1  = $_POST['userPassword1'];
    $userPassword2  = $_POST['userPassword2'];
    $userAccpted    = $_POST['userAccpted'];
    $userComment    = $_POST['userComment'];

    //If everything is ok...
    $userFirstName  = trim($connection->real_escape_string($userFirstName));
    $userMidInitial = trim($connection->real_escape_string($userMidInitial));
    $userLastName   = trim($connection->real_escape_string($userLastName));
    $userCountry    = trim($connection->real_escape_string($userCountry));
    $userState      = trim($connection->real_escape_string($userState));
    $userCity       = trim($connection->real_escape_string($userCity));
    $userZIP        = trim($connection->real_escape_string($userZIP));
    $userUsername   = trim($connection->real_escape_string($userUsername));
    $userEmail      = trim($connection->real_escape_string($userEmail));
    $userPassword1  = trim($connection->real_escape_string($userPassword1));
    $userPassword2  = trim($connection->real_escape_string($userPassword2));
    $userComment    = trim($connection->real_escape_string($userComment));

    //check if the email and the username are already used
    $check1 = $connection->query("SELECT * FROM accounts WHERE userName = '$userUsername'");
    $check2 = $connection->query("SELECT * FROM accounts WHERE userEmail = '$userEmail'");


    //Check userFirstName
    if (strlen($userFirstName) == 0) {
        $result['state'] = '<p style="color: red;">First Name cannot be empty</p>';
    } else if (!preg_match('/^([a-zA-Z0-9\s\_\-]+)$/', $userFirstName)) {
        $result['state'] = '<p style="color: red;">English letters, dashes, and underscores are only allowed in first names</p>';
    } else if ($userFirstName[0] == '_' || $userFirstName[0] == '-' || $userFirstName[strlen($userFirstName) - 1] == '_' || $userFirstName[strlen($userFirstName) - 1] == '-') {
        $result['state'] = '<p style="color: red;">Your first name must start and end with a letter</p>';
    }
    //check userMidInitial
    else if (strlen($userMidInitial) != 1) {
        $result['state'] = '<p style="color: red;">Middle initial needs to be exactly one charachter</p>';
    } else if (!preg_match('/^[a-zA-Z]+$/', $userMidInitial)) {
        $result['state'] = '<p style="color: red;">Middle initial needs to be in English only</p>';
    } else if (strlen($userLastName) == 0) {
        $result['state'] = '<p style="color: red;">Last name cannot be empty</p>';
    } else if (!preg_match('/^([a-zA-Z0-9\s\_\-]+)$/', $userLastName)) {
        $result['state'] = '<p style="color: red;">Your last name must start and end with a letter</p>';
    } else if ($userLastName[0] == '_' || $userLastName[0] == '-' || $userLastName[strlen($userLastName) - 1] == '_' || $userLastName[strlen($userLastName) - 1] == '-') {
        $result['state'] = '<p style="color: red;">Your last name must start and end with a letter</p>';
    } else if ($userCountry == 0) {
        $result['state'] = '<p style="color: red;">You need to choose a country</p>';
    } else if ($userState == 0) {
        $result['state'] = '<p style="color: red;">You need to choose a state</p>';
    } else if ($userCity == 0) {
        $result['state'] = '<p style="color: red;">You need to choose a city</p>';
    } else if (strlen($userZIP) == 0) {
        $result['state'] = '<p style="color: red;">ZIP code cannot be empty</p>';
    } else if (strlen($userZIP) < 5 || strlen($userZIP) > 10) {
        $result['state'] = '<p style="color: red;">ZIP code sould be 5 chars minimum and 10 chars maximum</p>';
    } else if (strlen($userUsername) == 0) {
        $result['state'] = '<p style="color: red;">Username cannot be empty</p>';
    } else if ($userUsername[0] == '_' || $userUsername[0] == '-' || $userUsername[strlen($userUsername) - 1] == '_' || $userUsername[strlen($userUsername) - 1] == '-') {
        $result['state'] = '<p style="color: red;">Username must start and end with a letter</p>';
    } else if (!preg_match('/^([a-zA-Z0-9\s\_\-]+)$/', $userUsername)) {
        $result['state'] = '<p style="color: red;">English letters, dashes, and underscores are only allowed in usernames</p>';
    } else if (strlen($userEmail) == 0) {
        $result['state'] = '<p style="color: red;">Email connot be empty</p>';
    } else if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
        $result['state'] = 'Invalid email format';
    } else if (strpos($userEmail, '@') <= 5) {
        $result['state'] = '<p style="color: red;">Email needs to be at least 6 chars before the @ sign</p>';
    } else if ($check1->num_rows && $check2->num_rows) {
        $result['state'] = '<p style="color: red;">The entered email and username are already in use</p>';
    } else if ($check1->num_rows) {
        $result['state'] = '<p style="color: red;">The entered username is already in use</p>';
    } else if ($check2->num_rows) {
        $result['state'] = '<p style="color: red;">The entered email is already in use</p>';
    } else if (strlen($userPassword1) == 0 || strlen($userPassword2) == 0) {
        $result['state'] = '<p style="color: red;">Passwords cannot be empty</p>';
    } else if ($userPassword1 != $userPassword2) {
        $result['state'] = '<p style="color: red;">Passwords don\'t match!</p>';
    }
    //There is not need for checking if the user accepted the website terms because it's won't be set originally
    else {
        $vKey     = md5(time() . $userName);
        $password = md5($userPassword1);
        $insert   = $connection->query("INSERT INTO `accounts`(`id`, `firstName`, `lastName`, `midInitial`, `city`, `zip`, `userName`, `userEmail`, `password`,
                `comments`, `dateRegistered`, `vKey`, `isActive`, `isAdmin`)
                VALUES (Null,'$userFirstName','$userLastName','$userMidInitial','$userCity','$userZIP','$userUsername','$userEmail','$password','$userComment', Null,'$vKey', 0, 0)");
        if ($insert) {
            $to      = $userEmail;
            $subject = "CPM | Email Verification";
            $message = "<a href='http://localhost/CPM/Pages/emailVerification2?vKey=$vKey'> Register Account </a>";
            $headers = "From: cpm.bwp501@gmail.com \r\n";
            $headers .= "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            mail($to, $subject, $message, $headers);
            $result['state'] = 'successfully';
            header('Location: ../Pages/emailVerification.php');
        } else {
            $result['state'] = $connection->error;
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>CPM | Sign Up</title>
</head>

<body>
    <form id="form" class="form" method="POST">
        <div class="container-fluid">
            <div class="Signup_Page">
                <div class="container inner-page" style="background: white; background: white; max-width: 650px; border-radius: 25px;">
                    <div style="margin: auto; min-height: 800px; width: 100%;">
                        <div class="for-row text-center">
                            <label>
                                <h2><b>Sign Up</b></h2>
                            </label>
                        </div>
                        <label>
                            <h4><b>Personal Info</b></h4>
                        </label>
                        <div class="form-row">
                            <!-- FirstName -->
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" placeholder="Enter First Name" name="userFirstName" value="<?php isset($_POST['userFirstName']) ?$_POST['userFirstName'] : '';?>" id="userFirstName" onchange="firstNameCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Middle Initial -->
                            <div class="form-group col-md-2 ">
                                <label>Middle Initial</label>
                                <input type="text" class="form-control" name="userMidInitial" id="userMidInitial" onchange="midInitialCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Last Name -->
                            <div class="form-group col-md-4">
                                <label>Last Name</label>
                                <input type="text" class="form-control" placeholder="Enter Last Name" name="userLastName" id="userLastName" onchange="lastNameCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- Location Related Fields -->
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <!-- Country -->
                                <label>Country</label>
                                <select class="custom-select my-1 mr-sm-2" name="userCountry" id="userCountry" onchange="countryCheck()">
                                    <option value="0">Choose your country...</option>
                                    <?php
                        				require_once('dbConnection.php');
                        				$countries = mysqli_query($connection,"SELECT * FROM country ORDER BY country");
                        				while($country = mysqli_fetch_assoc($countries)){
                        					echo "<option value='".$country['id']."'>".$country['country']."</option>";
                        				}
                    			     ?>
                                </select>
                                <br />
                                <small>Error Message</small>
                            </div>
                            <!-- State -->
                            <div class="form-group col-md-6 ">
                                <label>State/Province</label>
                                <select class="custom-select my-1 mr-sm-2" name="userState" id="userState" onchange="stateCheck()">
                                    <option value="0">Choose your state...</option>
                                </select>
                                <br />
                                <small>Error Message</small>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- City -->
                            <div class="form-group col-md-6 ">
                                <label>City</label>
                                <select class="custom-select my-1 mr-sm-2" name="userCity" id="userCity" onchange="cityCheck()">
                                    <option value="0">Choose your city...</option>
                                </select>
                                <br />
                                <small>Error Message</small>
                            </div>
                            <!-- ZIP Code -->
                            <div class="form-group col-md-6 ">
                                <label>ZIP Code</label>
                                <input type="text" class="form-control" placeholder="Enter a username" name="userZIP" id="userZIP" onchange="zipCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- <hr class="mt-2 mb-2"/> -->
                        <label>
                            <h4><b>Account Info</b></h4>
                        </label>
                        <div class="form-row">
                            <!-- Username -->
                            <div class="form-group col-md-6 ">
                                <label>Username</label>
                                <input type="text" class="form-control" placeholder="Enter a username" name="userUsername" id="userUsername" onchange="userNameCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Email -->
                            <div class="form-group col-md-6 ">
                                <label>Email Address</label>
                                <input type="eamil" class="form-control" placeholder="Enter an email" name="userEmail" id="userEmail" onchange="emailCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- Password 1 -->
                            <div class="form-group col-md-6 ">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="userPassword1" id="userPassword1" onKeyDown="javascript: var keycode = keyPressed(event); if(keycode==32){ alert('Password cannot contain spaces!'); document.getElementById('userPassword1').value =''; return false;}" onchange="checkPass1()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Password 2 -->
                            <div class="form-group col-md-6 ">
                                <label>Repeat Password</label>
                                <input type="password" class="form-control" placeholder="Let's check it!" name="userPassword2" id="userPassword2" onKeyDown="javascript: var keycode = keyPressed(event); if(keycode==32){ alert('Password cannot contain spaces!'); document.getElementById('userPassword2').value =''; return false;}" onchange="checkPass2()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- Comments -->
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Comments</label>
                                <textarea class="form-control" rows="2" cols="4" style="resize: none;" maxlength="100" placeholder="Got any comments?" name="userComment" id="userComment"></textarea>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- Accept -->
                        <div class="form-check" style="padding-top: 3px">
                            <input class="form-check-input" type="checkbox" style="top: 4.6px; left: 22px" name="userAccpted" value="1" id="userAccpted" onchange="acceptCheck()">
                            <label class="form-check-label">
                                I agree to the website <a href="#">terms and conditions</a>.
                            </label>
                            <i class="fas fa-check-circle"></i>
                            <i class="fas fa-exclamation-circle"></i>
                            <small>Error Message</small>
                        </div>
                        <hr class="mt-2 mb-2" width="0px" />
                        <?php  if(isset($result['state'])){?>
                        <div class="text-center" style="padding-bottom: 6px;">
                            <div class="card bg-light" style="padding-top: 3px; width: 465px; margin-right: auto; margin-left: auto;">
                                <div class="card-text" style="height: 25px;">
                                    <?php echo $result['state']; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="form-row text-center">
                            <div class="form-group col-md-6">
                                <!-- Reset -->
                                <input class="btn btn-outline-secondary" type="reset" value="Reset" style="width: 150px;margin:auto;" onclick="clearInputs()">
                            </div>
                            <div class="form-group col-md-6">
                                <!-- Submit -->
                                <button type="button" name="btnSubmit" class="btn btn-primary" name="submitForm" id="submitForm" onclick="finalSubmit()" style="width: 150px; margin:auto;"> Submit </button>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- Or Register -->
                            <div class="form-group col-md-12 text-center" style="color: black;">
                                Already have an account? <a href="../Pages/signin">Sign in here</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php include 'javascripts.php';?>
</body>

</html>
