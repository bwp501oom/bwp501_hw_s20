<!--** addCity.php **-->

<!--
   SVU - ITE - S20 - BWP501-Project
   Dr.Bassel ALKHATIB
   == User Management Panel ==
   //
   Participants:
   -mhd_hussam_109817
   -omar_108591
   -omar_116205
   //
-->

<?php
include('master.php');
include('dbConnection.php');
if (isset($_POST['stateChoosen']) && isset($_POST['countryChoosen']) && isset($_POST['cityToAdd'])) {
    if ($_POST['countryChoosen'] == 0) {
        $result['state'] = '<p style = "color: red;">Choose a country first</p>';
    } else if ($_POST['stateChoosen'] == 0) {
        $result['state'] = '<p style = "color: red;">Choose a state first</p>';
    } else {
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        $countryChoosen = $_POST['countryChoosen'];
        $stateChoosen   = $_POST['stateChoosen'];
        $cityToAdd      = $_POST['cityToAdd'];
        $mysqli         = new mysqli($serverName, $userName, $password, $dbName);
        $countryChoosen = $mysqli->real_escape_string($_POST['countryChoosen']);
        $stateChoosen   = $mysqli->real_escape_string($_POST['stateChoosen']);
        $cityToAdd      = $mysqli->real_escape_string($_POST['cityToAdd']);
        $cityToAdd      = trim($cityToAdd);
        $cityToAdd      = ucwords($cityToAdd);
        if (strlen($cityToAdd) === 0) {
            $result['state'] = '<p style = "color: red;">City name cannot be empty</p>';
        } else {
            if (isset($_POST['cityToAdd']) && !preg_match('/^([a-zA-Z0-9\s\_\-]+)$/', $_POST['cityToAdd'])) {
                $result['state'] = '<p style = "color: red;">City name needs to be in Englsih letters</p>';
            } else {
                //Get the name of the corresponding country
                $country = "SELECT * FROM country WHERE id = '$countryChoosen'";
                $countryRes = mysqli_query($connection, $country) or die(mysqli_error($connection));
                $row1 = mysqli_fetch_array($countryRes);
                // print_r($row1);

                //Get the name of the corresponding state
                $state = "SELECT * FROM state WHERE id = '$stateChoosen'";
                $stateRes = mysqli_query($connection, $state) or die(mysqli_error($connection));
                $row2 = mysqli_fetch_array($stateRes);
                // print_r($row2);

                //check if the city to be added already exists in the corresponding state - country
                $check = "SELECT * FROM city WHERE state_id = '$stateChoosen' AND city = '$cityToAdd'";
                $res   = mysqli_query($connection, $check);
                if ($res->num_rows) {
                    $result['state'] = '<p style = "color: red;"><b>' . $cityToAdd . '</b> is already added to ' . $row2['state'] . ' - ' . $row1['country'] . '</p>';
                } else {
                    //if it is not previously added, then insert it into the table
                    $sql = "INSERT INTO `city`(`id`, `state_id`, `city`) VALUES (NULL,'$stateChoosen', '$cityToAdd')";
                    if ($connection->query($sql) === TRUE) {
                        $result['state'] = '<p style = "color: green;"><b>' . $cityToAdd . '</b> has been added successfully to ' . $row2['state'] . ' - ' . $row1['country'] . '</p>';
                    } else {
                        $result['state'] = "Error: " . $sql . "<br>" . $connection->error;
                    }
                }
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <title> CPM | Add City </title>
</head>

<body>
    <div>
        <div class="container" style="background: white; background: white; max-width: 500px; min-height: 300px; padding-top: 20px; border-radius: 25px 25px 5px 5px;">
            <div class="form-row text-center">
                <div class="form-group col-md-12">
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addCountry.php">Add a Country</a>
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addState.php">Add a State</a>
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addCity.php">Add a City</a>
                </div>
            </div>
            <br />
            <form id="addCityForm" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>
                            <h5>Select a Country</h5>
                        </label>
                        <select class="custom-select my-1 mr-sm-2" name="countryChoosen" id="countryChoosen" onchange="countryChoosenforCity()">
                            <option value="0" hidden>Choose a country...</option>
                            <?php
                           require_once('dbConnection.php');
                           $countries = mysqli_query($connection,"SELECT * FROM country ORDER BY country");
                           while($country = mysqli_fetch_assoc($countries)){
                             echo "<option value='".$country['id']."'>".$country['country']."</option>";
                           }
                           ?>
                        </select>
                        <small>Error Message</small>
                    </div>
                    <div class="form-group col-md-12" style="margin-top: 5px;">
                        <label>
                            <h5> Select a State </h5>
                        </label>
                        <select class="custom-select my-1 mr-sm-2" name="stateChoosen" id="stateChoosen" onchange="stateChoosenCheck()">
                            <option value="0" hidden>Choose a state...</option>
                        </select>
                        <small>Error Message</small>
                    </div>
                    <div class="form-group custom col-md-12" style="margin-bottom: 25px; margin-top: 5px;">
                        <label>
                            <h5> Enter a City </h5>
                        </label>
                        <input type="text" class="form-control" placeholder="Enter the State Name" name="cityToAdd" id="cityToAdd" onchange="cityToAddCheck()">
                        <i class="fas fa-check-circle"></i>
                        <i class="fas fa-exclamation-circle"></i>
                        <small style="padding-left: 3px;"> Error Message</small>
                    </div>
                    <div class="col-md-12 text-center" style="padding-bottom: 15px;">
                        <button type="button" class="btn btn-primary" onclick="addCityS()"> Add City </button>
                    </div>
                </div>
            </form>
            <?php  if(isset($_POST['cityToAdd'])){?>
            <div class="text-center" style="padding-bottom: 6px;">
                <div class="card bg-light" style="padding-top: 3px;">
                    <div class="card-text" style="height: 25px;">
                        <?php echo $result['state']; ?>
                    </div>
                </div>
            </div>
            <?php
                }
              ?>
        </div>
    </div>
    </div>
    <?php
         include 'javascripts.php';
      ?>
</body>

</html>
