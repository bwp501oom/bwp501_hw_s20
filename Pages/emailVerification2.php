<!--
SVU - ITE - S20 - BWP501-Project
Dr.Bassel
== User Management Panel ==
//
Participants:
-mhd_hussam_109817
-omar_108591
-omar_116205
//
-->
<?php
include('master.php');
include('dbConnection.php');
$resultt = NULL;
$success = 0;
if (isset($_GET['vKey'])) {
    $vKey    = $_GET['vKey'];
    $result  = $connection->query("SELECT isActive, vKey FROM accounts WHERE isActive = 0 AND vKey = '$vKey' LIMIT 1");
    if ($result->num_rows == 1) {
        $update = $connection->query("UPDATE accounts SET isActive = 1 WHERE vKey = '$vKey' LIMIT 1");
        if ($update) {
            $resultt = '<div class="col-md-12 text-center content-header">
                         <h3><b>Your account has been verfied!</b></h3>
                        </div>
                        <div class="col-md-12 text-center content-header" style="padding-top: 15px;">
                         <h4><a href="../Pages/signin.php">Sign In</a></h4>
                        </div>';
            $success = 1;
        } else {
            $resultt = $connection->error;
            $success = 0;
        }
    } else {
        $resultt = '<div class="col-md-12 text-center content-header" style="color: red;">
                     <h3><b>This account is invalid or already verfied</b></h3>
                    </div>
                    <div class="col-md-12 text-center content-header" style="padding-top: 15px;">
                     <h4><a href="../Pages/signin.php">Sign In</a></h4>
                    </div>';
        $success = 0;
    }
} else {
    $resultt = '<div class="col-md-12 text-center content-header" style="color: red;">
                 <h3><b>This account is invalid or already verfied</b></h3>
                </div>
                <div class="col-md-12 text-center content-header" style="padding-top: 15px;">
                 <h4><a href="../Pages/signin.php">Sign In</a></h4>
                </div>';
    $success = 0;
}
?>

<!DOCTYPE html>
<html>

<head>
    <title> CPM | Email Verification </title>
</head>

<body>
    <div class="Signup_Page">
        <div class="col-md-6 inner-page" style="background: white; height: 180px; width: 650px; padding-top: 10px;">
            <div class="container-fluid">
                <div class="text-center" style="padding-bottom: 15px; padding-top: 15px;">
                    <img src="<?php if($success){echo "../Assets/Icons/checkedBox.svg";} else{echo "../Assets/Icons/wrong.svg";} ?>" alt="COPLogo" height="50px" width="70px" />
                </div>
                <div class="row">
                  <?php echo $resultt ?>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php include 'javascripts.php';?>
</body>

</html>
