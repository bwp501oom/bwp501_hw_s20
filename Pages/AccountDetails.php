<!DOCTYPE html>
<html>

<head>
    <title>CPM | Account Info</title>
    <?php
        include 'master.php';
        include('dbConnection.php');
    ?>
</head>

<body>
    <form id="form" class="form" method="POST">
        <div class="container-fluid">
            <div class="Signup_Page">
                <div class="container inner-page" style="background: white; background: white; max-width: 650px; border-radius: 25px;">
                    <div style="margin: auto; min-height: 800px; width: 100%;">
                        <div class="for-row text-center">
                            <label>
                                <h2><b>Update Your Account</b></h2>
                            </label>
                        </div>
                        <label>
                            <h4><b>Personal Info</b></h4>
                        </label>
                        <div class="form-row">
                            <!-- FirstName -->
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" placeholder="Enter First Name" name="userFirstName" id="userFirstName" onchange="firstNameCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Middle Initial -->
                            <div class="form-group col-md-2 ">
                                <label>Middle Initial</label>
                                <input type="text" class="form-control" name="userMidInitial" id="userMidInitial" onchange="midInitialCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Last Name -->
                            <div class="form-group col-md-4">
                                <label>Last Name</label>
                                <input type="text" class="form-control" placeholder="Enter Last Name" name="userLastName" id="userLastName" onchange="lastNameCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- Location Related Fields -->
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <!-- Country -->
                                <label>Country</label>
                                <select class="custom-select my-1 mr-sm-2" name="userCountry" id="userCountry" onchange="countryCheck()">
                                    <option value="0">Choose your country...</option>
                                    <?php
                                        require_once('dbConnection.php');
                                        $countries = mysqli_query($connection,"SELECT * FROM country ORDER BY country");
                                        while($country = mysqli_fetch_assoc($countries)){
                                            echo "<option value='".$country['id']."'>".$country['country']."</option>";
                                        }
                                     ?>
                                </select>
                                <br />
                                <small>Error Message</small>
                            </div>
                            <!-- State -->
                            <div class="form-group col-md-6 ">
                                <label>State/Province</label>
                                <select class="custom-select my-1 mr-sm-2" name="userState" id="userState" onchange="stateCheck()">
                                    <option value="0">Choose your state...</option>
                                </select>
                                <br />
                                <small>Error Message</small>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- City -->
                            <div class="form-group col-md-6 ">
                                <label>City</label>
                                <select class="custom-select my-1 mr-sm-2" name="userCity" id="userCity" onchange="cityCheck()">
                                    <option value="0">Choose your city...</option>
                                </select>
                                <br />
                                <small>Error Message</small>
                            </div>
                            <!-- ZIP Code -->
                            <div class="form-group col-md-6 ">
                                <label>ZIP Code</label>
                                <input type="text" class="form-control" placeholder="Enter a username" name="userZIP" id="userZIP" onchange="zipCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- <hr class="mt-2 mb-2"/> -->
                        <label>
                            <h4><b>Account Info</b></h4>
                        </label>
                        <div class="form-row">
                            <!-- Username -->
                            <div class="form-group col-md-6 ">
                                <label>Username</label>
                                <input type="text" class="form-control" placeholder="Enter a username" name="userUsername" id="userUsername" onchange="userNameCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Email -->
                            <div class="form-group col-md-6 ">
                                <label>Email Address</label>
                                <input type="eamil" class="form-control" placeholder="Enter an email" name="userEmail" id="userEmail" onchange="emailCheck()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- Password 1 -->
                            <div class="form-group col-md-6 ">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="userPassword1" id="userPassword1" onKeyDown="javascript: var keycode = keyPressed(event); if(keycode==32){ alert('Password cannot contain spaces!'); document.getElementById('userPassword1').value =''; return false;}" onchange="checkPass1()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                            <!-- Password 2 -->
                            <div class="form-group col-md-6 ">
                                <label>Repeat Password</label>
                                <input type="password" class="form-control" placeholder="Let's check it!" name="userPassword2" id="userPassword2" onKeyDown="javascript: var keycode = keyPressed(event); if(keycode==32){ alert('Password cannot contain spaces!'); document.getElementById('userPassword2').value =''; return false;}" onchange="checkPass2()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- Comments -->
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Comments</label>
                                <textarea class="form-control" rows="2" cols="4" style="resize: none;" maxlength="100" placeholder="Got any comments?" name="userComment" id="userComment"></textarea>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <hr class="mt-2 mb-2" width="0px" />
                        <div class="form-row text-center">
                            <div class="form-group col-md-6">
                                <!-- Reset -->
                                <input class="btn btn-outline-secondary btnDelete" type="button" value="Delete My Account" style="width: 150px;margin:auto;" onclick="deleteAccount()">
                            </div>
                            <div class="form-group col-md-6">
                                <!-- Submit -->
                                <input class="btn btn-primary" type="button" value="Update" style="width: 150px; margin:auto;" onclick="updateAccount(event)">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php include 'javascripts.php';?>
    <!-- to hash password -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/md5.js"></script>
    <script type="text/javascript">
        var PrevFirstName, PrevMidInitial, PrevLastName, PrevZip, PrevUsername, PrevEmail, PrevComment, PrevCity;
        var PasswordChanged = false;
        $().ready(function() {
            var userId = new URLSearchParams(window.location.search).get('id');
            try {
                var accJson = JSON.parse(atob(getCookie('BWP501Account')));
                if (accJson["id"] == userId || accJson["isAdmin"] == 1) {
                    getAccountDetails();
                }
                else {
                    alert("You do not have access to this page, redirecting ..");
                    window.location.replace("../Pages/Signin.php");
                }
            }
            catch (error){
                alert("You do not have access to this page, redirecting ..");
                window.location.replace("../Pages/Signin.php");
            }



        });


    </script>
</body>

</html>
