<!DOCTYPE html>
<html>

<head>
    <title>CPM | Home</title>
    <?php include 'master.php';?>
</head>

<body>
    <div class="container HomePage">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10" style="background-color: white;">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="AccountDiv row text-center">
                            <span class="AccountDivName">Logged in as: <a href="#" id="AccountName"></a></span>
                        </div>
                    </div>


                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" href="#">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>reg1.php?</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>

                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" href="../Pages/Signup.php">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>Sign Up</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>
                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" href="#" id="AccountDetails">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>Account Details</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>
                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" href="../Pages/addCountry.php">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>Add a Country</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>
                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" href="#">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>reg4.php</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>
                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" href="#">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>reg4.php</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>
                    <div class="col-lg-4 HomeLinkDiv">
                        <a class="HomeLink" onclick="LogOut();">
                            <img src="../Assets/Icons/Users.png" class="HomeLinkIcon" />
                            <span>Log out</span>
                            <img class="DownArrow" src="../Assets/Icons/DownArrow.png" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <?php include 'javascripts.php';?>
    <script type="text/javascript">
        $().ready(function() {
            try {
                var accJson = JSON.parse(atob(getCookie('BWP501Account')));
                $("#AccountName").attr("href","../pages/AccountDetails.php?id="+ accJson["id"]);
                $("#AccountName").text(accJson["firstName"] + " " + accJson["lastName"]);
                $("#AccountDetails").attr("href","../pages/AccountDetails.php?id="+ accJson["id"]);
            }
            catch (error){
                alert("You do not have access to this page, redirecting ..");
                window.location.replace("../Pages/Signin.php");
            }


        });



    </script>
</body>

</html>
