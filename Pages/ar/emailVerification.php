<!--
SVU - ITE - S20 - BWP501-Project
== User Management Panel ==
//
Participants:
-mhd_hussam_109817
-omar_108591
-omar_116205
//
Last time updated by Hussam Habbas on 20-OCT-29
-->
<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>CPM | التحقق من البريد</title>
      <!-- CSS file -->
      <?php
      include 'stylesheets.php';
      ?>
  </head>
  <body>
    <div class="text-center" style="padding-bottom: 15px; padding-top: 5px;">
      <img src="../../Assets\Icons\CPMLogo.svg" alt="COPLogo" height="70px" width="70px"/>
    </div>
        <div class="Signup_Page ar">
            <div class="col-md-6 inner-page" style="background: white; height: 270px; width: 650px; padding-top: 10px;">
              <div class="container-fluid">
                <div class="text-center" style="padding-bottom: 15px; padding-top: 15px;">
                  <img src="../../Assets\Icons\email.svg" alt="COPLogo" height="50px" width="70px"/>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center content-header">
                          <h2><b>!قم بتوثيق بريدك الإلكترونيّ </b></b></h2>
                          <h3>لقد تم إرسال بريد إلكتروني إلى بريدكم، يرجى اتباع الرابط لتوثيق الحساب</h3>
                    </div>
                </div>
                </div>
              </div>
        </div>
    </div>
    <?php include 'javascripts.php';?>
  </body>
</html>
