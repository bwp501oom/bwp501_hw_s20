<!--
SVU - ITE - S20 - BWP501-Project
== User Management Panel ==
//
Participants:
-mhd_hussam_109817
-omar_108591
-omar_116205
//
Last time updated by Hussam Habbas on 20-OCT-29
-->
<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title>CPM | تسجيل الدخول</title>
      <?php include 'stylesheets.php';?>
  </head>
  <body>
    <div class="text-center" style="padding-bottom: 15px; padding-top: 5px;">
      <img src="../../Assets\Icons\CPMLogo.svg" alt="COPLogo" height="70px" width="70px"/>
    </div>
    <div class="container-fluid">
        <div class="Signup_Page ar text-right">
            <div class="col-md-6 inner-page" style="background: white; height: 420px; width: 425px; padding-top: 10px;">
                <div class="row">
                    <div class="col-md-12 text-center content-header">
                        <span>
                          <h2>تسجيل الدخول</h2>
                        </span>
                    </div>
                </div>
                <div class="content-container-1" style="margin: auto; height: auto; width: 80%; padding: 10px;">
                    <h4>الإيميل/اسم المستخدم</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="اللإيميل">
                        </div>
                    </div>
                    <h4>كلمة المرور</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="كلمة المرور">
                        </div>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label" for="exampleCheck1">تذكرني</label>
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <br/>
                      <br/>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn-primary form-control" style="height:40px;"><h4>سجل الدخول</h4></button>
                      <br />
                    </div>
                    <div class="row">
                      <div class="col-md-12 text-center">
  		                    ليس لديك حساب؟<br /> <a href="signup.php">سجِّل هنا</a>
                      </div>
                    </div>
                </div><!-- content-container-1 Div -->
              </div>
        </div><!-- col-md-6 Div -->
    </div><!-- Container-fluid Div -->

    <?php include 'javascripts.php';?>
  </body>
</html>
