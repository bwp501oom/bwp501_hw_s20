<!--
SVU - ITE - S20 - BWP501-Project
== User Management Panel ==
//
Participants:
-mhd_hussam_109817
-omar_108591
-omar_116205
//
Last time updated by Hussam Habbas on 20-OCT-29
-->
<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <title> CPM | حساب جديد </title>
      <?php include 'stylesheets.php';?>
  </head>
  <body>
    <div class="text-center" style="padding-bottom: 15px; padding-top: 5px;">
      <img src="../../Assets\Icons\CPMLogo.svg" alt="COPLogo" height="70px" width="70px"/>
    </div>
    <div class="container-fluid">
        <div class="Signup_Page ar text-right">
            <div class="col-md-6 inner-page" style="background: white; height: 815px; width: 425px; padding-top: 10px;">
                <div class="row">
                    <div class="col-md-12 text-center content-header">
                        <span>
                          <h2>حساب جديد</h2>
                        </span>
                    </div>
                </div>
                <div class="content-container-1" style="margin: auto; height: 635px; width: 80%; padding: 10px;">
                    <h4>الإسم الأول</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="FirstName" class="input_style">
                        </div>
                    </div>
                    <h5>إسم الأب (أول حرف)</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="MiddleInitial" class="input_style" style="width: 100px;">
                        </div>
                    </div>
                    <h4>إسام العائلة</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="LastName" class="input_style">
                        </div>
                    </div>
                    <h4>اسم المستخدم</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="Email" class="input_style" style="text-align:right;" placeholder="ادخل اسما">
                        </div>
                    </div>
                    <div class="form-group">
                    <h4>البريد الإلكتروني</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="Email" class="input_style">
                        </div>
                    </div>
                    <h4>كلمة المرور</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="Password" class="input_style">
                        </div>
                    </div>
                    <h4>إعادة كلمة المرور</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="RepeatPassword" class="input_style">
                        </div>
                    </div>
                </div><!-- content-container-1 Div -->

                <div class="content-container-2" style="margin: auto; height: 635px; width: 80%; padding: 10px;">
                  <label for="country"><h4>البلد</h4></label>
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select class="form-control" style="height: auto; text-align:right;" id="country-dropdown">
                              <option value="" selected disabled hidden>أختر بلدك</option>
                              <?php
                                require_once "db.php";
                                $result = mysqli_query($conn,"SELECT * FROM countries");
                                while($row = mysqli_fetch_array($result)) {
                                ?>
                                <option value="<?php echo $row['id'];?>"><?php echo $row["name"];?></option>
                                <?php
                                }
                                ?>
                            </select>
                          </div>
                        </div>
                    </div>
                    <h4>الولاية / المحافظة</h4>
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select class="form-control" id="state-dropdown" style="height: auto;">
                              <option value="" selected disabled hidden>إختر الولاية / المحافظة</option>
                            </select>
                          </div>
                        </div>
                    </div>
                    <h4>المدينة</h4>
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select class="form-control" id="city-dropdown" style="height: auto;">
                              <option value="" selected disabled hidden>أختر المدينة</option>
                            </select>
                          </div>
                        </div>
                    </div>
                    <h4>الرمز البريدي</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="ZipCode" class="input_style">
                        </div>
                    </div>
                    <h4>تعليقات إضافية</h4>
                    <div class="row">
                        <div class="col-md-12">
                           <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="هل لديك أي تعليق؟" rows="4" style="resize: none; width: 300px; text-align:right;" maxlength="100"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h5><input type="checkbox" name="Iagree" style=" height: 15px; width: 15px;"> انا أقبل بـ <a href="#">شروط</a> الموقع</h5>
                        </div>
                    </div>
                </div><!-- content-container-2 Div -->
                <div style="width: 450px; margin: auto; width: 80%; padding-top: 4px;">
                  <div class="row">
                      <div class="col-md-12 text-center dots">
                          <span class="dot" style="height: 22px; width: 22px;"></span>
                          <span class="dot-inactive" style=" height: 22px; width: 22px;"></span>
                      </div>
                  </div>
                  <div class="content-container-1">
                      <div class="row">
                          <div class="col-md-12 text-center">
                              <input type="button" name="SignupNext" value="التالي" id="SignupNextBtn" style="width: 80px; height: 45px;">
                          </div>
                      </div>
                  </div>
                  <div class="content-container-2">
                      <div class="row">
                          <div class="col-md-12 text-center">
                              <input type="button" name="SignupNext" value="تسجيل" id="SignupSubmitBtn" style="width: 100px; height: 45px;">
                              <input type="button" name="SignupNext" value="السابق" id="SignupPreviousBtn" style="width: 100px; height: 45px; margin-right: 20px;">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 text-center" style="color: white;">
                      هل لديك حساب؟ <a href="signin.php" style="color: white;">سجل الدخول هنا</a>
                    </div>
                  </div>
                </div>
              </div><!-- Signup_Page Div -->
          </div><!-- col-md-6 Div -->
      </div><!-- Container-fluid Div -->
    </div>
    <?php include 'javascripts.php';?>
  </body>
</html>
