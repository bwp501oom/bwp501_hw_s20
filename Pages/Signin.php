<!--
   SVU - ITE - S20 - BWP501-Project
   Dr.Bassel ALKHATIB
   == User Management Panel ==
   //
   Participants:
   -mhd_hussam_109817
   -omar_108591
   -omar_116205
   //
-->
<?php
include('master.php');
include('dbConnection.php');
if (isset($_POST['userEmail']) && isset($_POST['enteredPassword'])) {
    $userName          = $connection->real_escape_string($_POST['userEmail']);
    $password          = $connection->real_escape_string($_POST['enteredPassword']);
    $password          = md5($password);
    $resultByUserName  = $connection->query("SELECT id,firstName,lastName,isActive,isAdmin FROM `accounts` WHERE userName = '$userName' AND password = '$password' LIMIT 1");
    $resultByUserEmail = $connection->query("SELECT id,firstName,lastName,isActive,isAdmin FROM `accounts` WHERE userEmail = '$userName' AND password = '$password' LIMIT 1");
    if ($resultByUserName->num_rows != 0) {
        $row      = $resultByUserName->fetch_assoc();
        $isActive = $row['isActive'];
        if ($isActive) {
            createCookie($row);
        } else {
            $result['state'] = 'This account hasn\'t been activated yet';
        }
    } else if ($resultByUserEmail->num_rows != 0) {
        $row      = $resultByUserEmail->fetch_assoc();
        $isActive = $row['isActive'];
        if ($isActive) {
            createCookie($row);
        } else {
            $result['state'] = '<p style="color:red;">This account hasn\'t been activated yet</p>';
        }
    } else {
        $result['state'] = '<p style="color:red;">The username/email or password is incorrect</p>';
    }
}
function createCookie($account) {
    $account = base64_encode(json_encode($account));
    setcookie("BWP501Account", $account, time()+3600,"/");
    header('Location: ../Pages/HomePage.php');
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>CPM | Sign In</title>
</head>

<body>
    <form id="sform" class="sform" method="POST">
        <div class="container-fluid">
            <div class="Signup_Page">
                <div class="container inner-page" style="background: white; background: white; max-width: 450px; border-radius: 25px;">
                    <div style="margin: auto; min-height: 350px; width: 100%;">
                        <div class="for-row text-center" style="padding:12px">
                            <label>
                                <h2><b>Sign In</b></h2>
                            </label>
                        </div>
                        <div class="form-row">
                            <!-- Email/Username -->
                            <div class="form-group col-md-12 ">
                                <label>Email/Username</label>
                                <input type="eamil" class="form-control" placeholder="Enter an email" name="userEmail" id="enteredEmail">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- Password -->
                            <div class="form-group col-md-12 ">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="enteredPassword" id="enteredPassword" onchange="checkPass()">
                                <i class="fas fa-check-circle"></i>
                                <i class="fas fa-exclamation-circle"></i>
                                <small>Error Message</small>
                            </div>
                        </div>
                        <!-- Remember Me -->
                        <div class="form-check" style="padding-top: 5px">
                            <input class="form-check-input" type="checkbox" style="top: 4.6px; left: 22px" name="userAccptedN" id="rememberMe">
                            <label class="form-check-label">
                                Remember me
                            </label>
                        </div>
                        <hr class="mt-2 mb-2" width="0px" />
                        <div class="form-row text-center">
                            <div class="form-group col-md-12">
                                <!-- Submit -->
                                <input class="btn btn-primary" type="button" value="Submit" style="width: 150px; margin:auto;" onclick="signInSubmit()">
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- Or Sign up -->
                            <div class="form-group col-md-12 text-center" style="color: black;">
                                Haven't got an account yet?<br /> <a href="../Pages/signup">Register Here</a>
                            </div>
                        </div>

                        <div class="form-row">
                          <?php  if(isset($result['state'])){?>
                          <div class="form-group col-md-12 text-center" style="padding-bottom: 6px;">
                              <div class="card bg-light" style="padding-top: 3px;">
                                  <div class="card-text" style="height: 25px;">
                                      <?php echo $result['state']; ?>
                                  </div>
                              </div>
                          </div>
                          <?php
                              }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php include 'javascripts.php';?>
</body>

</html>
