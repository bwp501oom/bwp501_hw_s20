<!--
   SVU - ITE - S20 - BWP501-Project
   Dr.Bassel ALKHATIB
   == User Management Panel ==
   //
   Participants:
   -mhd_hussam_109817
   -omar_108591
   -omar_116205
   //
   -->
   <?php
   include('master.php');
   include('dbConnection.php');
   if (isset($_POST['stateToAdd']) && isset($_POST['countryChoosen'])) {
       if ($_POST['countryChoosen'] == 0) {
           $result['state'] = '<p style = "color: red;">Choose a country first</p>';
       } else {
           if ($connection->connect_error) {
               die("Connection failed: " . $connection->connect_error);
           }
           $countryChoosen = $_POST['countryChoosen'];
           $stateToAdd     = $_POST['stateToAdd'];
           $mysqli         = new mysqli($serverName, $userName, $password, $dbName);
           $countryChoosen = $mysqli->real_escape_string($_POST['countryChoosen']);
           $stateToAdd     = $mysqli->real_escape_string($_POST['stateToAdd']);
           $stateToAdd     = trim($stateToAdd);
           $stateToAdd     = ucwords($stateToAdd);
           if (strlen($stateToAdd) === 0) {
               $result['state'] = '<p style = "color: red;">State name cannot be empty</p>';
           } else {
               if (isset($_POST['stateToAdd']) && !preg_match('/^([a-zA-Z0-9\s\_\-]+)$/', $_POST['stateToAdd'])) {
                   $result['state'] = '<p style = "color: red;">State name needs to be in Englsih</p>';
               } else {
                   $country = "SELECT * FROM country WHERE id = '$countryChoosen'";
                   $countryRes = mysqli_query($connection, $country) or die(mysqli_error($connection));
                   $row   = mysqli_fetch_array($countryRes);
                   //print_r($row);
                   $check = "SELECT * FROM state WHERE country_id = '$countryChoosen' AND state = '$stateToAdd'";
                   $res   = mysqli_query($connection, $check);
                   if ($res->num_rows) {
                       $result['state'] = '<p style = "color: red;"><b>' . $stateToAdd . '</b> is already added </p>';
                   } else {
                       $sql = "INSERT INTO `state`(`id`, `country_id`, `state`) VALUES (NULL, '$countryChoosen','$stateToAdd')";
                       if ($connection->query($sql) === TRUE) {
                           $result['state'] = '<p style = "color: green;"><b>' . $stateToAdd . '</b> has been added successfully to ' . $row['country'] . '</p>';
                       } else {
                           $result['state'] = "Error: " . $sql . "<br>" . $connection->error;
                       }
                   }
               }
           }
       }
   }
   ?>

<!DOCTYPE html>
<html>

<head>
    <title> CPM | Add State </title>
</head>

<body>
    <div>
        <div class="container" style="background: white; background: white; max-width: 500px; min-height: 300px; padding-top: 20px; border-radius: 25px 25px 5px 5px;">
            <div class="form-row text-center">
                <div class="form-group col-md-12">
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addCountry.php">Add a Country</a>
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addState.php">Add a State</a>
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addCity.php">Add a City</a>
                </div>
            </div>
            <br />
            <form id="addStateForm" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-12" style="padding-bottom: 15px;">
                        <label>
                            <h5>Select a Country</h5>
                        </label>
                        <select class="custom-select my-1 mr-sm-2" name="countryChoosen" id="countryChoosenS" onchange="countryChoosenforState()">
                            <option value="0" hidden>Choose a country...</option>
                            <?php
                         require_once('dbConnection.php');
                         $countries = mysqli_query($connection,"SELECT * FROM country ORDER BY country");
                         while($country = mysqli_fetch_assoc($countries)){
                           echo "<option value='".$country['id']."'>".$country['country']."</option>";
                         }
                        ?>
                        </select>
                        <small>Error Message</small>
                    </div>
                    <div class="form-group custom col-md-12" style="padding-bottom: 15px;">
                        <label>
                            <h5> Enter a State </h5>
                        </label>
                        <input type="text" class="form-control" placeholder="Enter the State Name" name="stateToAdd" id="stateToAdd" onchange="stateToAddCheck()">
                        <i class="fas fa-check-circle"></i>
                        <i class="fas fa-exclamation-circle"></i>
                        <small>Error Message</small>
                    </div>
                    <div class="col-md-12 text-center" style="padding-bottom: 15px;">
                        <button type="button" class="btn btn-primary" onclick="addStateS()"> Add State </button>
                    </div>
                </div>
            </form>
            <?php  if(isset($_POST['stateToAdd']) && isset($_POST['countryChoosen'])){?>
            <div class="text-center" style="padding-bottom: 6px;">
                <div class="card bg-light" style="padding-top: 3px;">
                    <div class="card-text" style="height: 25px;">
                        <?php echo $result['state']; ?>
                    </div>
                </div>
            </div>
            <?php
                }
              ?>
        </div>
    </div>
    </div>
    <?php
         include 'javascripts.php';
      ?>
</body>

</html>
