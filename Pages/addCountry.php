<!--
   SVU - ITE - S20 - BWP501-Project
   Dr.Bassel ALKHATIB
   == User Management Panel ==
   //
   Participants:
   -mhd_hussam_109817
   -omar_108591
   -omar_116205
   //
   -->
   <?php
   include('master.php');
   include('dbConnection.php');

   /* You can use this regex expression for nums, characters, -, and _ ==> /^[\w-]+$/*/
   if (isset($_POST['countryToAdd'])) {
       if ($connection->connect_error) {
           die("Connection failed: " . $connection->connect_error);
       }
       $mysqli      = new mysqli($serverName, $userName, $password, $dbName);
       $countryName = $_POST['countryToAdd'];
       $countryName = $mysqli->real_escape_string($_POST['countryToAdd']);
       $countryName = trim($countryName);
       $countryName = ucwords($countryName);
       if (strlen($countryName) === 0) {
           $result['state'] = '<p style = "color: red;">Country name cannot be empty</p>';
       } else {
           if (isset($_POST['countryToAdd']) && !preg_match('/^([a-zA-Z0-9\s\_\-]+)$/', $_POST['countryToAdd'])) {
               $result['state'] = '<p style = "color: red;">Country name needs to be in Englsih letters</p>';
           } else {
               $check = "SELECT * FROM country WHERE country = '$countryName'";
               $res   = mysqli_query($connection, $check);
               if ($res->num_rows) {
                   $result['state'] = '<p style = "color: red;"><b>' . $countryName . '</b> is already added to countries</p>';
               } else {
                   $sql = "INSERT INTO `country`(`id`, `country`) VALUES (NULL ,'$countryName')";
                   if ($connection->query($sql) === TRUE) {
                       $result['state'] = '<p style = "color: green;"><b>' . $countryName . '</b> has been added successfully</p>';
                   } else {
                       $result['state'] = "Error: " . $sql . "<br>" . $connection->error;
                   }
                   // header('Location: ctyX.php');
               }
           }
       }
   }
   ?>

<!DOCTYPE html>
<html>

<head>
    <title> CPM | Add Country </title>
</head>

<body>
    <div>
        <div class="container" style="background: white; background: white; max-width: 500px; min-height: 200px; padding-top: 20px; border-radius: 25px 25px 5px 5px;">
            <div class="form-row text-center">
                <div class="form-group col-md-12">
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addCountry.php">Add a Country</a>
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addState.php">Add a State</a>
                    <a class="btn btn-primary" style="width: 120px;" href="../Pages/addCity.php">Add a City</a>
                </div>
            </div>
            <br />
            <form id="addCountryForm" method="POST">
                <div class="form-row">
                    <div class="form-group custom col-md-12">
                        <label>
                            <h5>Country Name</h5>
                        </label>
                        <input type="text" class="form-control" placeholder="Enter the Country Name" name="countryToAdd" id="countryToAdd" onchange="addCountryCheck()">
                        <i class="fas fa-check-circle"></i>
                        <i class="fas fa-exclamation-circle"></i>
                        <small style="padding-left: 3px;">Error Message</small>
                    </div>
                    <div class="col-md-12 text-center" style="margin-top: 10px; padding-bottom: 10px;">
                        <button type="button" class="btn btn-primary" onclick="addCountryS()"> Add country </button>
                    </div>
                </div>
            </form>
            <?php  if(isset($result['state'])){?>
            <div class="text-center" style="padding-bottom: 6px;">
                <div class="card bg-light" style="padding-top: 3px;">
                    <div class="card-text" style="height: 25px;">
                        <?php echo $result['state']; ?>
                    </div>
                </div>
            </div>
            <?php
                }
              ?>
        </div>
    </div>
    </div>
    <?php
         include 'javascripts.php';
         $result = '';
         ?>
</body>

</html>
