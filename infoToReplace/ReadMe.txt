-First:
* Go to your XAMPP folder and search for a folder called 'sendmail'.
* Open it and then search for a file called 'sendmail.ini' and replace it with the one provided here. 

-Second: 
* Go to your XAMPP folder and search for a folder called 'php'.
* Open it and then search for a file called 'php.ini' and replace it with the one provided here.
Note that inside the replaced 'php.ini' you need to check if the 'sendmail_path' is correct, the default one that
I have added is 'sendmail_path =  "D:\xampp\sendmail\sendmail.exe -t"', so replace it according to where you have 
your XAMPP installed. 